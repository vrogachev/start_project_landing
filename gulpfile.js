var gulp           = require('gulp'),
    gutil          = require('gulp-util' ),
    sass           = require('gulp-sass'),
    browserSync    = require('browser-sync'),
    concat         = require('gulp-concat'),
    uglify         = require('gulp-uglify'),
    svgSprite 	   = require('gulp-svg-sprite'),
    svgmin         = require('gulp-svgmin'),
    cheerio        = require('gulp-cheerio'),
    replace        = require('gulp-replace'),
    cssmin         = require('gulp-cssmin'),
    cleanCSS       = require('gulp-clean-css'),
    rename         = require('gulp-rename'),
    clean          = require('gulp-clean'),
    del            = require('del'),
    imagemin       = require('gulp-imagemin'),
    pngquant       = require('imagemin-pngquant'),
    cache          = require('gulp-cache'),
    autoprefixer   = require('gulp-autoprefixer'),
    bourbon        = require('node-bourbon'),
    ftp            = require('vinyl-ftp'),
    notify         = require('gulp-notify'),
    plumber		   = require('gulp-plumber'),
    panini         = require('panini'),
    cssfont64      = require('gulp-cssfont64');
    series         = require('gulp-series');


var assetsDir = 'app/';
var outputDir = 'dist/';
// Панини
gulp.task('pages', function() {
    return gulp.src('app/pages/**/*.{html,hbs,handlebars}')
        .pipe(panini({
            root: 'app/pages/',
            layouts: 'app/layouts/',
            partials: 'app/partials/',
            data: 'app/data/',
            helpers: 'app/helpers/'
        }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
});

// Обновления статуса панини
gulp.task('resetPages', function() {
    panini.refresh();
});

// авторелоадер браузера
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'dist'
        },
        notify: false
    });
});

gulp.task('svgSprite', function () {
    return gulp.src('./app/img/sprite/*.svg')
    // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "./site/dist/img/sprite.svg",
                    dest:    '/lg/',
                    render: {
                        scss: {
                            dest:'./site/app/sass/_sprite.scss',
                            template: "app/sass/templates/_template.scss"
                        }
                    }
                }
            }
        }))
        .pipe(gulp.dest('./site/dist/'));
});

// Sass
gulp.task('sass', function() {
    return gulp.src('app/sass/**/*.scss')
        .pipe(sass({
            includePaths: bourbon.includePaths
        }).on("error", notify.onError()))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}))
});

// Конкатенация js
gulp.task('libs', function() {
    return gulp.src([
        'app/libs/jquery/js/jquery.min.js',
        'app/libs/swiper/dist/js/swiper.min.js',
        'app/libs/magnific-popup/dist/jquery.magnific-popup.js',
        'app/libs/svg4everybody-master/dist/svg4everybody.js'
    ])
        .pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(rename('libs.min.js'))
        .pipe(gulp.dest('dist/js'))
});

// Выгрузка в dist
gulp.task('distfile', function () {

    var buildFonts = gulp.src('app/fonts/**/**.*')
        .pipe(gulp.dest('dist/fonts'));

    var buildJs = gulp.src('app/js/**/**.*')
        .pipe(gulp.dest('dist/js'));

});


gulp.task('compress', function() {
    gulp.src(['app/img/static/**/*.jpg', 'app/img/static/**/*.png'])
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'))
});


gulp.task('fontsConvert', function () {
    return gulp.src([assetsDir + 'fonts/*.woff', assetsDir + 'fonts/*.woff2'])
        .pipe(cssfont64())
        .pipe(gulp.dest(outputDir + 'fonts/'))
        .pipe(browserSync.stream());
});


gulp.task('watch', ['sass', 'browser-sync', 'svgSprite', 'distfile', 'libs'], function() {
    gulp.watch('app/sass/**/*.scss', ['sass']);
    gulp.watch('app/js/**/*.js', browserSync.reload);
    gulp.watch('app/img/sprite/**/*', ['svgSprite'], browserSync.reload);
    gulp.watch(['app/img/static/**/*.jpg', 'app/img/static/**/*.png'], ['compress']);
    gulp.watch('app/js/**/*', ['distfile']);
    gulp.watch(assetsDir + 'fonts/**/*', ['fontsConvert']);
    gulp.watch('app/libs/**/*', ['libs', 'sass'], browserSync.reload);
    gulp.watch('app/{layouts,pages,partials}/**/*.html', ['resetPages', 'pages'], browserSync.reload);
});

gulp.task('default', ['watch'], function () {});
